/*global jQuery:true, $:true */
$(document).ready(function () {
    'use strict';

    $('img')
        .removeAttr('height')
        .removeAttr('width');
});