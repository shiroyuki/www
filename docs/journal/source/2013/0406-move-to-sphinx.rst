Move to Sphinx
==============

:Written in: April 6, 2013
:Tags: :doc:`/tags/updates`

As I usually write topics related to programming and photography, I decided to customize
`Sphinx <http://sphinx-doc.org>`_ for writing blogs from now on.

Sphinx is a library and software that I use for technical documentation. In term of blogging,
the disadvantages are:

* manual media and content management,
* manual publishing and deployment.

However, the big advantages are:

* using the powerful ReStructure text (RST) which allows me to share code or mathematical statements without hassle,
* no database involved as it relies on RST files,
* fast as the whole site is compiled into HTML files,
* portable as everything is compiled,
* no security issue like SQL injection,
* no annoying issue like spamming,
* almost full code customization.

Does this mean that I will ditch my old blog? Nope. You should be able to visit my old blog
at http://journal.shiroyuki.com (for the most recent entries) and http://nikki.shiroyuki.com
(for very old entries).

.. image:: https://lh6.googleusercontent.com/-_Lr3Ib0Th3s/UWC-gRASS0I/AAAAAAAACXY/8jV-tjkiNWw/s747/DSC_2260.jpg