Develop CLIs with Tori Framework
================================

:Written in: June 24, 2013
:Tags: :doc:`/tags/development`

A few years ago, I have plans to develop a lot of projects. The problem is
mostly about code organization, limitation or restriction imposed by frameworks.
I am glad that I finally develop Tori Framework to the point that it can be used
to solve those problems.

However, one useful concept that I get from Symfony Framework 2 is its **console**
command framework. So, I want to develop something similar or better in Python
(as defined `GitHub Issue #29 <https://github.com/shiroyuki/Tori/issues/29>`_).

I currently posted the prototype as a part of NEP codebase (`see prototype <https://github.com/nepteam/nep/blob/master/neptune/console>`_)
where a command class will be implemented with two method:

* define_arguments
* execute

like the following example.

.. code-block:: python

    class HelloWorldCommand(object):
        """ Load fixture """
        def __init__(self):
            pass

        def define_arguments(self, argument_parser):
            argument_parser.add_argument('-f', '--file', action='store_true')

        def execute(self, args):
            if args.file:
                with open('/tmp/hello-world.txt', 'w') as f:
                    f.write('hello world')

                return

            print('hello world')

Then, the commands can be registered like this.

.. code-block:: xml

    <entity
        id="hello-world"
        class="neptune.command.Database"
        tags="command command:hello-world">
    </entity>

To be honest, with this, I can be more productive and care less about developing
something from scratch every time I need to do some data management where the UI
is not ready.

(NEP is an open-source educational project described on `Google Doc <https://docs.google.com/document/d/1qgx5SkqTG61OdDOBerSNrojMqPGVMtlQkIPLEugbTqg/edit?usp=sharing>`_.)