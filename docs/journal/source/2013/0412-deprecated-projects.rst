Deprecated Projects in 2013
===========================

The following projects will be trashed by June 2013 but the project names will be recycled.

* Council Project due to http://openbadges.org/.
* GherkinCS as it will be replaced soon with PHP implementation.
* Pint Project and Pints due to the insufficient developmental resources.

.. note::

    Some code from Council Prject will be merged to
    `Tori Extra Bundle <https://github.com/shiroyuki/tori-bundle>`_.

