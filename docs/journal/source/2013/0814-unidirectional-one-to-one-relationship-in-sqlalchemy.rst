Unidirectional One-to-one Relationship in SQLAlchemy
====================================================

:Written in: April 6, 2013
:First edition: August 26, 2012
:Tags: :doc:`/tags/development`, :doc:`/tags/diary`, :doc:`/tags/python`, :doc:`/tags/sqlalchemy`, :doc:`/tags/architecture`

.. note::

    2013.08.14: Reposted the article originally published at http://journal.shiroyuki.com/2012/08/unidirectional-one-to-onerelationship.html

.. note::

    2012.09.16: I presented this topic at PyCon JP 2012 (an unscheduled 30-minute
    session) in Tokyo. The presentation can be downloaded from my
    `Google Drive <https://docs.google.com/file/d/0B08sZ8OD-ZQfWGRacW9LQklScVE/edit?usp=sharing>`_.

.. note::

    2012.08.26: So, after I posted this originally, I found problem that the child
    was not persisted. So I revisited the problem with primaryjoin and it worked.

SQLAlchemy is a wonderful ORM for Python, except the documentation. It took me
hours spending on reading documentation and experiments. The official
documentation mostly emphasizes how to do bidirectional one-to-one relationship
but I couldn't find the unidirectional one.

After my effort to unearth the "undocumented" feature, here is my finding.

Based on the documentation, supposed these entities were in different files and
I wanted to create a **unidirectional one-to-one relationship** from ``Parent``
to ``Child``. This meant ONLY ``Parent`` knows about ``Child`` but not vise versa.
However, both examples created different problems.

The first example potentially causes circular dependencies. This terminology may
sound cool to many people but it isn't fun at all as this kind of problem may
introduce infinite recursion. No one want to have this problem in any programming
languages. If you wonder how it might happen, try to add the third entity, namely
``Tree``, from the third module into the equation. Now, we have the perfect harmony
of circular dependencies. (As far as I can tell, Python automatically throw an
error to prevent this scenario. But, I won't rely on that.)

The second one still cause bidirectional relationship. Even though I can't see
the cycle by code, I still see the cycle when the object is instantiated or
mapped by SQLAlchemy.

The root of all problem is due to ``backref``. as it instructs the session
to make bi-directional relationship whether or not you like it.

To make the actual unidirectional one-to-one relationship from ``Parent`` to
``Child``, based on the second example, I just need to write:

.. code-block:: python

    child = relationship('Child', primaryjoin='Parent.child_id == Child.id').

That's it. End of story.