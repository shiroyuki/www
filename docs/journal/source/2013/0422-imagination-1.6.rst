Imagination 1.6 Released
========================

.. image:: http://farm9.staticflickr.com/8173/8041617640_560d253e14_b_d.jpg

:Written in: April 22, 2013
:Tags: :doc:`/tags/releases`

I proudly announce the available release of Imagination which allows everyone
to write code in the aspect-oriented programming way.

In 2012, the project is formally introduced during a 5-minute talk at PyCon JP,
2012, as the focus is on the development of the object relational mapping for
MongoDB as a part of `Tori Framework <http://shiroyuki.com/work/project-tori>`_,
the development of Imagination is mainly to support the coming release of Tori
Framework (version 2.1). Nevertheless, there are a few highlights worthly
mentioned.

New features?
-------------

* Support ``list``, ``tuple``, and ``set`` in the XML schema. [#ghi12]_
* Support ``dict`` in the XML schema. [#ghi13]_

As usual, the updated schema of the configuration can be found on the main
documentation site [#idoc]_.

.. rubric:: Reference

.. [#idoc] https://imagination.readthedocs.org/en/latest/api/helper.assembler.html
.. [#ghi12] https://github.com/shiroyuki/Imagination/issues/12
.. [#ghi13] https://github.com/shiroyuki/Imagination/issues/13