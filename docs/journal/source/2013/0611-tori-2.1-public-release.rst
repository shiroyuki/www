Tori 2.1.0 Public Release
=========================

:Written in: June 11, 2013
:Tags: :doc:`/tags/releases`, :doc:`/tags/tori`

Finally, after having done research and studied on the implementation pattern
for more than 6 months, I proudly present Tori 2.1 which is greatly emphasized
on simplifying the routing configuration for the micro web framework part
and introducing the proper ORM form MongoDB.

The general public release of Tori 2.1 is now available where the key features are:

* `The Object-relational Mapping interface (ORM) <http://tori.readthedocs.org/en/latest/manual/orm/index.html>`_
* `The Simple Routing <http://tori.readthedocs.org/en/latest/manual/configuration/routing.html#simple-routing-pattern>`_

You should be able to install by::

    sudo pip install --upgrade tori

or check out the source code from the GitHub and run::

    make install

If the documentation is incomplete, does not make any sense to you, or the code
might have an issue, please feel free to file a bug or, even better, send a pull
request on https://github.com/shiroyuki/Tori.

If you have any question, please feel free to ask me on either
`Twitter <http://twitter.com/shiroyuki>`_ or `Google+ <https://plus.google.com/102676332273506890611/about>`_.
