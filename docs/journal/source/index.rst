.. Journal documentation master file, created by
   sphinx-quickstart on Thu Oct 25 22:57:45 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Home
****

Feature
=======

.. include:: 2013/0611-tori-2.1-public-release.rst
    :end-line: 16

Read more from :doc:`2013/0611-tori-2.1-public-release`.

.. include:: 2013/0422-imagination-1.6.rst
    :end-line: 17

Read more from :doc:`2013/0422-imagination-1.6`.

Something else?
===============

Check out the :doc:`sitemap`

.. toctree::
    :maxdepth: 1
    :glob:
    :titlesonly:
    :hidden:

    sitemap
    tags/index
    20*/index
    misc/index

..  Indices and tables
    ==================

    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`

