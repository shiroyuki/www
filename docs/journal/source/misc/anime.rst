Anime
*****

Spring 2013
===========

Followed
--------

* Shingeki no Kyojin / 進撃の巨人 (Saturday)
* Majestic Prince (Thursday)
* Ore no Imōto ga Konnani Kawaii Wake ga Nai. (Saturday)
* Suisei no Gargantia / Gargantia on the Verdurous Planet (Sunday)

Pending
-------

* Mushibugyō (8, Mon)
* Hayate (8, Mon)
* Aiura (9, Tue)
* Toaru Kagaku no Railgun S (12, Fri)
* Henneko (13, Sat)

BD Releases
===========

This is the purchase list sorted by priority.

* From Up On Poppy Hill / コクリコ坂から (TIFF, 2012)
* Psycho Pass (2013)
* Robotics;Notes (2013)