Manga
*****

MIX
===

:Worth to collect: Yes

* Author: Adachi Mitsuru

政宗くんのリベンジ
==============================================

:Worth to collect: Yes

* English: Masamune-kun no Revenge
* Author: Takeoka Hazuki
* Characters:
    * Masamune Makabe
    * Adagaki Aki

赤髪の白雪姫
==============================================

:Worth to collect: Yes

* English: Akagami no Shirayuki Hime
* Author: Akizuki Sorata
* Characters:
    * ゼン・ウィスタリア・クラリネス
    * 白雪（しらゆき）
    * ミツヒデ・ルーエン
    * 木々・セイラン（きき・－）
    * オビ

変態王子と笑わない猫。
==============================================

:Worth to collect: Yes

* English: The "Hentai" Prince and the Stony Cat.
* Author: Sagara Sou
* Characters:
    * Yokodera Youto
    * Tsutsukakushi Tsukiko