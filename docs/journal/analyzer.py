import codecs
from glob import glob
import os
import re

base_path      = os.path.dirname(os.path.abspath(__file__))
file_paths     = glob(os.path.join(base_path, 'source/201*/[0-9]*.rst'))
rx_tag_line    = re.compile('\:Tags\:([^\n]+)')
rx_tag_spliter = re.compile('\s*,\s*')
rx_tag_name    = re.compile(':doc:`/tags/([^`]+)`')
rx_doc_prefix  = re.compile('.+/source/')
tag_map        = {}
tag_base_path  = os.path.join(base_path, 'source/tags')
tag_template   = """
{tag_header}
{tag_marker}

{tag_desciption}

.. toctree::
   :maxdepth: 1

   {doc_list}
"""

for file_path in file_paths:
    with codecs.open(file_path) as f:
        code    = f.read()
        matches = rx_tag_line.search(code)

    tag_line = (matches.groups()[0]).strip() if matches else None

    if not tag_line:
        continue

    tags = rx_tag_spliter.split(tag_line)

    for tag in tags:
        matches = rx_tag_name.match(tag)

        if not matches:
            continue

        tag_name = matches.groups()[0]

        if tag_name not in tag_map:
            tag_map[tag_name] = []

        tag_map[tag_name].append(file_path)

for file_path in glob(tag_base_path + '/*.rst'):
    if 'index.rst' in file_path:
        continue

    os.unlink(file_path)

for tag in tag_map:
    file_paths = list(tag_map[tag])

    tag_header = tag
    tag_marker = '*' * len(tag_header)
    tag_desciption = '{} article(s) tagged with {}'.format(len(file_paths), tag)

    file_paths.sort()
    file_paths.reverse()

    doc_refs = [rx_doc_prefix.sub('/', file_path) for file_path in file_paths]

    with codecs.open(os.path.join(tag_base_path, tag + '.rst'), 'w') as f:
        f.write(tag_template.format(
            tag_header = tag_header,
            tag_marker = tag_marker,
            tag_desciption = tag_desciption,
            doc_list = '\n   '.join(doc_refs)
        ))