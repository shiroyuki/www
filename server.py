''' Server bootstrap '''
from logging import ERROR, WARNING, DEBUG, INFO
from os import environ
from threading import Thread
from subprocess import call
from sys import exit, argv
import time

from tori.application import Application
from tori.common      import LoggerFactory

class JournalMaker(Thread):
    def run(self):
        call(['touch', 'docs.lock'])
        call(['python', 'docs/journal/analyzer.py'])
        call(['make', 'doc'])
        call(['rm', 'docs.lock'])

if len(argv) < 2:
    print('{} <server|heroku>'.format(argv[0]))
    exit(15)

if '--disable-rst-compilation' not in argv:
    JournalMaker().start()

LoggerFactory.instance().set_default_level(INFO if '--dev' in argv else ERROR)

application = Application('config/app-{}.xml'.format(argv[1]))
application.listen(environ.get('PORT', 8000))
application.start()
