function mover_tooltip(event) {
    var self = this;
    var mx = event.pageX;
    var my = event.pageY;
    
    if ($("#sstt").length == 0) {
        $("body").append('<div id="sstt" style="opacity: 0.9; color: white; border:1px solid white; position:absolute; background-color: black; -moz-border-radius:5px; -webkit-border-radius:5px;"><div id="sstt-content" style="padding: 10px;"></div></div>');
    }
    
    var message = $(self).attr("cachedtitle");
    
    if (message.match(/Permanent Link/)) {
        message = "How about start reading?";
    }
    
    $("#sstt-content").text(message);
    
    $("body").mousemove();
    /*$("#sstt").css("top", my + 5);
    $("#sstt").css("left", mx + 5);*/
}

function mmove_tooltip(event) {
    if ($("#sstt").length == 0) {
        return;
    }
    
    var rx = "left";
    var ry = "top";
    var cx = parseInt($("#sstt").width());
    var cy = parseInt($("#sstt").height());
    var wx = parseInt($(window).width());
    var wy = parseInt($(window).height());
    var mx = event.pageX;
    var my = event.pageY;
    var ax = 5;
    var ay = 5;
    
    if (cx + mx + ax + 10 > wx) {
        mx -= cx;
        ax *= -1;
    }
    
    if (cy + my + ay + 10 > wy) {
        my -= cy;
        ay *= -1;
    }
    
    //console.log("Position: " + (cx + mx) + "/" + wx + ", " + (cy + my) + "/" + wy);
    
    $("#sstt").css(rx, mx + ax);
    $("#sstt").css(ry, my + ay);
}

function mout_tooltip(event) {
    if ($("#sstt").length > 0) {
        $("#sstt").remove();
    }
}

$(document).ready(function() {
    $("a[title]").each(function(index) {
        $(this).attr("cachedtitle", $(this).attr("title"));
        $(this).removeAttr("title");
        $(this).hover(mover_tooltip, mout_tooltip);
        $(this).find("[title], [alt]").each(function(index) {
            $(this).removeAttr("title");
            $(this).removeAttr("alt");
        });
    });
    
    $("body").bind("mousemove", mmove_tooltip);
});
