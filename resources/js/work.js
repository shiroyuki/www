var projects = [];
var range    = {
    begin: getPresentYear(),
    end:   getAlternativePresentYear(),
    diff:  0
};
var selector = {};

function Project(name, id, begin, end) {
    begin = parseInt(begin);
    end   = parseInt(end);

    var ongoing = end == getPresentYear();

    if (ongoing) {
        end = getAlternativePresentYear();
    }

    if (begin == end) {
        end = end + 1;
    }

    var repr = {
        name:     name,
        id:       id,
        begin:    begin,
        end:      end,
        duration: end - begin,
        ongoing:  ongoing
    };

    return repr
}

function gatherDataFromArticle(i) {
    var thisArticle = $(this);
    var project     = new Project(
        thisArticle.find('h3').text(),
        thisArticle.attr('data-id'),
        thisArticle.attr('data-begin'),
        thisArticle.attr('data-end')
    );

    if (project.begin < range.begin) {
        range.begin = project.begin;
        range.diff  = range.end - range.begin;
    }

    projects.push(project);
}

function DataViewer(projects, range) {
    var padding   = {
        edge:  25,
        range: 20
    };
    var dimension = {
        w: 920,
        h: 500,
    };
    var rect  = {
        h: 20,
        w: (dimension.w - padding.edge * 2) / (range.diff),
        r: 10
    };
    var headerOffset = selector.portfolio.eq(0).parent().outerHeight();
    var renderingOpts = {
        'fill':   '#666',
        'stroke': 'transparent'
    };
    var activeRenderingOpts = {
        'fill':   '#cc0000',
        'stroke': 'transparent'
    };
    var bgTextAttrs = {fill: '#ccc'};

    var paper = Raphael('history', dimension.w, dimension.h);
    var projectRangeBoxes = {};
    var currentId = null;

    function drawBackground() {
        var y1 = padding.edge / 2;
        var y2 = dimension.h - padding.edge / 2;

        for (var i = range.begin; i <= range.end + 1; i++) {
            var index = i - range.begin;
            var x     = rect.w * index + padding.edge;

            var path = paper.path([
                'M' + x + ',' + y1,
                'L' + x + ',' + y2,
                'Z'
            ].join(''));

            path.attr({
                'stroke':         '#ddd',
                'stroke-opacity': 1,
                'stroke-width':   0.5
            });

            paper.text(x, y1, String(i)).attr(bgTextAttrs);
            paper.text(x, y2, String(i)).attr(bgTextAttrs);
        }
    }

    function initiallyDrawOneRangeBox(count) {
        var beginning_year = this.begin - range.begin;

        var x = Math.floor(rect.w * beginning_year) + padding.edge;
        var y = Math.floor(rect.h + padding.range) * count + padding.edge;
        var w = Math.floor(rect.w * this.duration);

        var projectRangeBox = paper.rect(x, y, w, rect.h, rect.r);

        projectRangeBox.attr(renderingOpts);
        projectRangeBox.data('id', this.id);
        projectRangeBoxes[this.id] = projectRangeBox;

        var name          = $.trim(this.name);
        var nameMaxLength = 12 * (range.end - this.begin);

        if (name.length > nameMaxLength) {
            name = name.substring(0, nameMaxLength) + '...';
        }

        paper.text(x + rect.r, y + rect.h * 1.25, name).attr({'text-anchor': 'start'});

        // Event handler
        projectRangeBox.mouseover(function() {
            selector.timeline.addClass('change-cursor');
        });
        projectRangeBox.mouseout(function() {
            selector.timeline.removeClass('change-cursor');
        });
        projectRangeBox.click(onBoxClick);

        count++;
    }

    function onBoxClick(event) {
        var self = this;
        var id   = self.data('id');

        if (currentId !== null && currentId === id) {
            self.attr(renderingOpts);
            selector.pointer.hide();
            selector.portfolio.hide();

            currentId = null;

            return;
        }

        currentId = id;

        // [SVG]
        // Reset the color of other ranges.
        $.each(projectRangeBoxes, function(k, b) {
            this.attr(renderingOpts);
        });
        // Set the color of the activated one.
        self.attr(activeRenderingOpts);

        // [HTML]
        var target = getTarget(id);
        displayInfoBox(target, event);

        var newY = parseInt(target.offset().top) - 100;

        selector.body.animate({scrollTop: newY}, 1000);
    }

    function getTarget(id) {
        return selector.portfolio.children('article[data-id="' + id + '"]').eq(0);
    }

    function displayInfoBox(target, e) {
        eventPointer = e;
        var position = {
            x: e.offsetX
                ? e.offsetX
                : (e.clientX - 175),
            y: e.offsetY
                ? (parseInt(headerOffset) + e.offsetY + 15)
                : (e.clientY - 60)
        };

        // Reset the visual extras on other sections.
        selector.portfolio.hide();
        selector.article.not(target).removeClass('selected').hide();

        target.addClass('selected').show();

        target.parent().show();

        selector.pointer.css({
            left: position.x,
            top:  position.y
        });

        target.parent().css({
            left: position.x > 460 ? 460 : 0,
            top:  position.y + 15
        });

        if ( ! target.find('.image').hasClass('loaded')) {
            target.find('.image').addClass('loaded');

            var targetImage    = target.find('.image > span');
            var targetImageUrl = targetImage.text();

            if (targetImage.length == 0) {
                target.find('.image').remove();
            } else {
                targetImage.replaceWith('<img src="' + targetImageUrl + '"/>');
            }
        }

        selector['pointer' + (position.x % 460 > 230 ? 'R' : 'L')].show();
        selector['pointer' + (position.x % 460 > 230 ? 'L' : 'R')].hide();

        return target;
    }

    function initiallyDrawAllRangeBoxes() {
        drawBackground();
        $.each(projects, initiallyDrawOneRangeBox);
    }

    return {
        initiallyDrawAllRangeBoxes: initiallyDrawAllRangeBoxes,
        cancelPanel: function() {
            selector.pointer.hide();
            selector.portfolio.hide();

            $.each(projectRangeBoxes, function(k, b) {
                this.attr(renderingOpts);
            });
        }
    };
}

$(document).ready(function() {
    selector.body      = $('html,body');
    selector.timeline  = $('#history');
    selector.portfolio = $('.annual-portfolio');
    selector.article   = $('.annual-portfolio > article[data-id]');
    selector.pointer   = $('.pointer');
    selector.pointerL  = $('#data-pointer-l');
    selector.pointerR  = $('#data-pointer-r');
    selector.anchor    = $('.commands a');
    selector.container = $('section[data-mode]');
    selector.mode_switch = $('a[href^=#p]');

    selector.portfolio.hide();
    selector.article.each(gatherDataFromArticle);

    // Draw Timeline
    var dv = new DataViewer(projects, range);
    dv.initiallyDrawAllRangeBoxes();

    // Draw Pointer
    var pointerAttrs = {fill: '#333', stroke: 'transparent'};

    var pointerPaperL = Raphael('data-pointer-l', 15, 15);
    var pointer = pointerPaperL.path('M0,0L15,15L0,15,L0,0');
    pointer.attr(pointerAttrs);

    var pointerPaperR = Raphael('data-pointer-r', 15, 15);
    var pointer = pointerPaperR.path('M15,0L15,15L0,15,L15,0');
    pointer.attr(pointerAttrs);

    // Event Listeners
    selector.mode_switch.click(function (e) {
        e.preventDefault();

        var self = $(this);

        selector.container.attr('data-mode', self.attr('href').substring(1));
        selector.body.animate({scrollTop: 0}, 0);
    })

    selector.anchor.click(function (e) {
        var isPermanentLink = ! String($(this).attr('href')).match(/^https?:\/\//);
        var isCloseButton   = $(this).hasClass('close');

        if (isCloseButton) {
            dv.cancelPanel();
        } else if (isPermanentLink) {
            e.preventDefault();
            alert('Do the right-click on the link.');
            return;
        }
    });

    selector.container.attr('data-mode', 'projects');
});
