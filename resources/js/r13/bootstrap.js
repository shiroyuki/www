LogoDrawer = function () {};

LogoDrawer.prototype.draw = function (targetElement, eyeSize, mouthSize) {
    var context = targetElement.getContext('2d'),
        mouthTopX = targetElement.width / 2,
        mouthTopY = targetElement.height * 2 / 3 - mouthSize / 2,
        mouthBottomXLeft = mouthTopX - mouthSize * Math.cos(Math.PI / 6),
        mouthBottomXRight = mouthTopX + mouthSize * Math.cos(Math.PI / 6),
        mouthBottomY = targetElement.height * 2 / 3 + mouthSize * Math.sin(Math.PI / 6) + mouthSize / 2;
    
    context.fillStyle = '#000';

    context.beginPath();
    context.arc(targetElement.width / 3, targetElement.height / 3, eyeSize, 0, 360);
    context.fill();

    context.beginPath();
    context.arc(targetElement.width * 2 / 3, targetElement.height / 3, eyeSize, 0, 360);
    context.fill();

    context.beginPath();
    context.moveTo(mouthTopX, mouthTopY);
    context.lineTo(mouthBottomXLeft, mouthBottomY);
    context.lineTo(mouthBottomXRight, mouthBottomY);
    context.fill();
};

function addLogo() {
    var brandAnchor = document.querySelector('.navi.main .brand'),
        brandLogo   = document.createElement('canvas'),
        brandLabel  = document.createElement('span'),
        logoDrawer  = new LogoDrawer()
    ;

    brandLabel.classList.add('brand-label');
    brandLabel.innerHTML = brandAnchor.innerHTML;

    brandLogo.classList.add('brand-logo');
    brandLogo.height = 30;
    brandLogo.width = 45;

    logoDrawer.draw(brandLogo, 3, 4);

    brandAnchor.innerHTML = '';
    brandAnchor.appendChild(brandLogo);
    brandAnchor.appendChild(brandLabel);
}

function addContactInformation() {
    var footer   = document.querySelector('footer'),
        email    = document.createElement('a'),
        twitter  = document.createElement('a'),
        linkedin = document.createElement('a')
    ;
    
    email.href = "mailto:jnopporn@shiroyuki.com";
    email.innerHTML = "email me";
    
    twitter.href = "https://twitter.com/shiroyuki";
    twitter.innerHTML = "twitter";
    
    linkedin.href = "http://ca.linkedin.com/in/jnopporn/";
    linkedin.innerHTML = "linked in"
    
    email.classList.add('segment');
    twitter.classList.add('segment');
    linkedin.classList.add('segment');
    
    footer.appendChild(email);
    footer.appendChild(twitter);
    footer.appendChild(linkedin);
}

function startUp() {
    addLogo();
    addContactInformation();
}

////// Startup Script //////
$(document).ready(startUp);