# -*- coding: utf-8 -*-

from base64 import b64encode as encode, b64decode as decode
import codecs
from glob   import glob
from os     import listdir, path
import re

class MDDocumentService(object):
    __supported_exts = ['html', 'md']

    def __init__(self, finder, location):
        self.__finder     = finder
        self.__location   = location

    @property
    def recent_entries(self):
        files = glob(path.join(self.__location, '*'))

        files.sort()
        files.reverse()

        entries = []

        for location in files:
            id = re.sub('\.(md|html)', '', path.basename(location))

            entries.append({
                'id': id,
                'content': self.get(id)
            })

        return entries

    def get(self, id):
        file_path = path.join(self.__location, id)

        complete_file_path = None

        for ext in self.__supported_exts:
            complete_file_path = '{}.{}'.format(file_path, ext)

            if path.exists(complete_file_path):
                break

            complete_file_path = None

        if not complete_file_path:
            return None

        content = self.__finder.read(complete_file_path)

        return content
