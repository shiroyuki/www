import math
import re
from kotoba import load_from_file
from tori.db.entity import Entity
from app.model import WorkProject

class PortfolioService(object):
    """ Data Service for Past Projects """
    def __init__(self, location):
        self._location = location
        self._years    = []
        self._projects = {}
        self._project_list = []
        self.total     = 0
        self.mid_point = 0

    @property
    def years(self):
        if not self._years:
            self._load()

        return self._years

    @property
    def projects(self):
        if not self._projects:
            self._load()

        return self._projects

    @property
    def project_list(self):
        if not self._project_list:
            self._load()

        return self._project_list

    def _load(self):
        xml      = load_from_file(self._location)
        projects = {}

        for p in xml.find('project'):
            self.total += 1

            media_list = WorkProject.make_media_list(p.children('media').children())
            tag_list   = WorkProject.make_tags(p.children('tag').data())
            project    = WorkProject(
                p.attribute('name'),
                p.children('employer').data(),
                p.attribute('begin'),
                p.attribute('end'),
                p.children('url').data(),
                tag_list,
                media_list
            )

            self._years.append(project.end)

            if project.end not in projects:
                projects[project.end] = []

            projects[project.end].append(project)
            self._project_list.append(project)

        self._years = list(set(self.years))
        self._years.reverse()

        for y in self.years:
            self._projects[y] = projects[y]

        self.mid_point = int(math.ceil(self.total / 2))

class ProjectService(object):
    """ Data Service for Current Projects """
    _active_scopes = ['active', 'collaboration']
    _re_desc       = re.compile('(\s*\n\s*){2,}')

    def __init__(self, location):
        self.location = location
        self.parser = None
        self.cache  = {}

    def get(self, id):
        if id in self.cache:
            return self.cache[id]

        if not self.parser:
            self.parser = load_from_file(self.location)

        query_id = '[id={}]'.format(id)

        subparser = self.parser.children(query_id)

        if not subparser:
            raise KeyError('{} is not registered'.format(id))

        project  = subparser[0]

        if project.attribute('scope') not in self._active_scopes:
            return None

        returnee = {
            'id':                   project.attribute('id'),
            'name':                 project.attribute('name'),
            'scope':                project.attribute('scope'),
            'description':          self._re_desc.sub('</p><p>', project.children('description').data()),
            'url_to_source_code':   project.children('src').data(),
            'url_to_download':      project.children('dl').data(),
            'url_to_documentation': project.children('doc').data(),
            'show_logo':            project.attribute('logo') != 'off',
            'project_scope':        project.attribute('scope')
        }

        self.cache[id] = Entity(**returnee)

        return self.cache[id]
