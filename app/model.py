from re     import split
from time   import time, sleep
from kotoba import load_from_file

class WorkProjectTag(object):
    def __init__(self, name):
        self.name = name.strip().capitalize()

class WorkProjectMedia(object):
    def __init__(self, kind, content):
        self.kind    = kind
        self.content = content.strip()

class WorkProject(object):
    last_id                  = 0
    present_year             = 2100
    alternative_present_year = 2013

    def __init__(self, name, employer, begin, end, url, tags=[], media_list=[]):
        self.id    = WorkProject.last_id + 1
        self.name  = name
        self.employer = employer
        self.begin = int(begin)
        self.url   = url
        self.tags  = tags
        self.media_list = media_list

        WorkProject.last_id = self.id

        try:
            self.end = int(end)
        except ValueError:
            self.end = end == 'same' and self.begin or self.present_year # 2100 means present.

    @staticmethod
    def make_media_list(media_in_dom_list):
        processed_media_list = []

        for media_block in media_in_dom_list:
            processed_media = WorkProjectMedia(media_block.name(), media_block.data())
            processed_media_list.append(processed_media)

        return processed_media_list

    @staticmethod
    def make_tags(tags_in_string):
        unprocessed_tags = split('\s*,\s*', tags_in_string)
        processed_tags   = []

        for processed_tag in unprocessed_tags:
            processed_tag = WorkProjectTag(processed_tag)
            processed_tags.append(processed_tag)

        return list(set(processed_tags))

class Portfolio(object):
    def __init__(self):
        self.years    = []
        self.projects = {}

    def load(self, location):
        xml      = load_from_file(location)
        projects = {}

        for p in xml.find('project'):
            media_list = WorkProject.make_media_list(p.children('media').children())
            tag_list   = WorkProject.make_tags(p.children('tag').data())
            project    = WorkProject(
                p.attribute('name'),
                p.children('employer').data(),
                p.attribute('begin'),
                p.attribute('end'),
                p.children('url').data(),
                tag_list,
                media_list
            )

            self.years.append(project.end)

            if project.end not in projects:
                projects[project.end] = []

            projects[project.end].append(project)

        self.years = list(set(self.years))
        self.years.reverse()

        for y in self.years:
            self.projects[y] = projects[y]

