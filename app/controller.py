# -*- coding: utf-8 -*-
from   os.path import basename, exists, join as path_join
from   glob    import glob
import httplib
import re
import traceback

from kotoba          import load_from_file
from kotoba.fs       import read as fread
from tornado.web     import HTTPError
from tori.centre     import settings, services
from tori.controller import Controller, ResourceService
from tori.controller import SimpleController as BaseSimpleController
from tori.controller import ErrorController as BaseErrorController
from tori.decorator.controller import _assign_renderer, _enable_custom_error, custom_error, renderer

from app.model import Portfolio, WorkProject

# Override the resource controller to work with Sphinx-based Journal

def resource_is_journal_locked(self):
    return re.search('^/journal/', self.request.path) and exists('docs.lock')

def resource_error_handler(self, status_code, **kwargs):
    debug_info = []
    is_journal = self.is_journal_locked()

    if self.settings.get("debug") and "exc_info" in kwargs:
        # in debug mode, try to send a traceback
        for line in traceback.format_exception(*kwargs["exc_info"]):
            debug_info.append(line)

    if is_journal:
        self.set_status(200)

    self.render(
        'error.html',
        is_journal = is_journal,
        code       = status_code,
        message    = httplib.responses[status_code],
        debug_info = ''.join(debug_info)
    )

def resource_request_handler(self, path=None):
    if self.is_journal_locked():
        return self.render(
            'error.html',
            is_journal = is_journal
        )
    self._get(path)

_assign_renderer(ResourceService, 'app.view')

ResourceService._get = ResourceService.get
ResourceService.get  = resource_request_handler
ResourceService.write_error       = resource_error_handler
ResourceService.is_journal_locked = resource_is_journal_locked

# End of customization

class ControllerMixin(object):
    @property
    def is_supagarn_code(self):
        return 'supagarn' in self.request.host

@custom_error('r13/error.html')
@renderer('app.view')
class SimpleController(BaseSimpleController, ControllerMixin):
    def rendering_prefix(self):
        return 'supagarn.ac.th' if self.is_supagarn_code else 'r13'

@custom_error('r13/error.html')
@renderer('app.view')
class WorkController(Controller, ControllerMixin):
    def get(self, project_id = None):
        contexts = {}

        if project_id:
            project = None

            try:
                project = self.component('projects').get(project_id)
            except KeyError as e:
                raise HTTPError(405)

            if not project:
                raise HTTPError(404)

            return self.render('r13/work/single-project.html', project = project)

        contexts['portfolio'] = self.component('portfolio')
        contexts['projects'] = self.component('projects')

        self.render('r13/work/index.html', **contexts)

@custom_error('r13/error.html')
@renderer('app.view')
class ErrorController(BaseErrorController):
    ''' Based on https://gist.github.com/398252 '''
