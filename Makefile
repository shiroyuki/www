process: doc
	pip install --force-reinstall https://bitbucket.org/shiroyuki/ossarchive/downloads/tori-2.2.0a.tar.gz
	python server.py heroku

css:
	@sass --update resources/scss:resources/css --style compressed

css_live:
	@sass --watch resources/scss:resources/css

doc:
	cd docs/journal && make clean && make html
